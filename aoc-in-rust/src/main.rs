mod part_01;

fn main() {
    println!("========== PART 01 ==========");
    part_01::main();
    println!("=============================");
}
