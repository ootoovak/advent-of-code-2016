fn split_into_direction_components(direction: &str) -> (&str, &str) {
    direction.split_at(1)
}

fn distance_as_a_number(distance: &str) -> i32 {
    distance.parse().unwrap()
}

enum Orientation {
    North,
    South,
    West,
    East,
}

use self::Orientation::*;

fn blocks_to_destination(full_directions: &str) -> i32 {
    let directions = full_directions.split(", ")
        .map(|direction| split_into_direction_components(direction))
        .map(|(orientation, distance)| (orientation, distance_as_a_number(distance)))
        .scan(North, |state, (mut orientation, mut distance)| {
            match state {
                &mut North => {
                    if orientation == "L" {
                        distance = distance * -1;
                        *state = West;
                    } else {
                        *state = East;
                    }
                    orientation = "H";
                }
                &mut South => {
                    if orientation == "L" {
                        *state = East;
                    } else {
                        distance = distance * -1;
                        *state = West;
                    }
                    orientation = "H";
                }
                &mut West => {
                    if orientation == "L" {
                        distance = distance * -1;
                        *state = South;
                    } else {
                        *state = North;
                    }
                    orientation = "V";
                }
                &mut East => {
                    if orientation == "L" {
                        *state = North;
                    } else {
                        distance = distance * -1;
                        *state = South;
                    }
                    orientation = "V";
                }
            }

            Some(*state)
        })
        .inspect(|element| println!("> {}", element));
    0
}

pub fn main() {
    let full_directions =
        "L1, L3, L5, L3, R1, L4, L5, R1, R3, L5, R1, L3, L2, L3, R2, R2, L3, L3, R1, L2, R1, L3, \
         L2, R4, R2, L5, R4, L5, R4, L2, R3, L2, R4, R1, L5, L4, R1, L2, R3, R1, R2, L4, R1, L2, \
         R3, L2, L3, R5, L192, R4, L5, R4, L1, R4, L4, R2, L5, R45, L2, L5, R4, R5, L3, R5, R77, \
         R2, R5, L5, R1, R4, L4, L4, R2, L4, L1, R191, R1, L1, L2, L2, L4, L3, R1, L3, R1, R5, \
         R3, L1, L4, L2, L3, L1, L1, R5, L4, R1, L3, R1, L2, R1, R4, R5, L4, L2, R4, R5, L1, L2, \
         R3, L4, R2, R2, R3, L2, L3, L5, R3, R1, L4, L3, R4, R2, R2, R2, R1, L4, R4, R1, R2, R1, \
         L2, L2, R4, L1, L2, R3, L3, L5, L4, R4, L3, L1, L5, L3, L5, R5, L5, L4, L2, R1, L2, L4, \
         L2, L4, L1, R4, R4, R5, R1, L4, R2, L4, L2, L4, R2, L4, L1, L2, R1, R4, R3, R2, R2, R5, \
         L1, L2";
    let distance_in_blocks = blocks_to_destination(full_directions);

    println!("Directions where: {}", full_directions);
    println!("The distance in blocks to the destination is: {}",
             distance_in_blocks);
}

#[cfg(test)]
mod tests {

    use super::process;

    #[test]
    fn first_example() {
        let test_input = "R2, L3";
        assert_eq!(5, part00(test_input));
    }

    #[test]
    fn second_example() {
        let test_input = "R2, R2, R2";
        assert_eq!(2, part00(test_input));
    }

    #[test]
    fn third_example() {
        let test_input = "R5, L5, R5, R3";
        assert_eq!(12, part00(test_input));
    }

}
